package com.example.grocerygo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class BoodschappenlijstAdapter extends RecyclerView.Adapter<BoodschappenlijstAdapter.ProductViewHolder> {

    //this context we will use to inflate the layout
    private Context mCtx;

    //we are storing all the products in a list
    private List<BoodschappenlijstCard> productList;

    //getting the context and product list with constructor
    public BoodschappenlijstAdapter(Context mCtx, List<BoodschappenlijstCard> productList) {
        this.mCtx = mCtx;
        this.productList = productList;
    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //inflating and returning our view holder
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.boodschappenlijst_card, null);

        mCtx = parent.getContext();

        return new ProductViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ProductViewHolder holder, int position) {
        //getting the product of the specified position
        final BoodschappenlijstCard product = productList.get(position);

        //binding the data with the viewholder views
        holder.textViewTitle.setText(product.getTitle());
        holder.textViewShortDesc.setText(product.getDescription());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), BoodschappenlijstActivity.class);
                Bundle b = new Bundle();
                b.putInt("id", product.getId()); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                ((Activity) mCtx).startActivityForResult(intent, 100);
            }
        });
    }


    @Override
    public int getItemCount() {
        return productList.size();
    }


    class ProductViewHolder extends RecyclerView.ViewHolder {

        TextView textViewTitle, textViewShortDesc;

        public ProductViewHolder(View itemView) {
            super(itemView);

            textViewTitle = itemView.findViewById(R.id.Title);
            textViewShortDesc = itemView.findViewById(R.id.Description);
        }
    }
}