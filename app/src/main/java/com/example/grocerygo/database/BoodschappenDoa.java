package com.example.grocerygo.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface BoodschappenDoa {

    @Query("SELECT * FROM Boodschappenlijst")
    public List<Boodschappenlijst> getBoodschappenlijst();

    @Query("SELECT * FROM Boodschappenlijst WHERE id = :id")
    public Boodschappenlijst getBoodschappenlijstById(int id);

    @Insert
    public void insert(Boodschappenlijst... items);
    @Update
    public void update(Boodschappenlijst... items);
    @Delete
    public void delete(Boodschappenlijst item);

    @Query("SELECT * FROM Boodschappenlijst")
    List<Boodschappenlijst> loadAll();

}
