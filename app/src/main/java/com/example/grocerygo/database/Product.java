package com.example.grocerygo.database;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

@Entity
public class Product {
    @PrimaryKey(autoGenerate = true)
    private int id;
    private int boodschappenlijstId;
    private String title;
    @Nullable
    private String description;
    @Nullable
    private double price;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getBoodschappenlijstId() {
        return boodschappenlijstId;
    }

    public void setBoodschappenlijstId(int boodschappenlijstId) {
        this.boodschappenlijstId = boodschappenlijstId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}