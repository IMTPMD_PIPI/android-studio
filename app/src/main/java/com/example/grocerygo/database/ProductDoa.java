package com.example.grocerygo.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ProductDoa {

    @Query("SELECT * FROM Product WHERE boodschappenlijstId = :id")
    public List<Product> getProductsById(int id);

    @Query("SELECT * FROM Product WHERE id = :id")
    public Product getProductById(int id);

    @Insert
    public void insert(Product... items);
    @Update
    public void update(Product... items);
    @Delete
    public void delete(Product item);
}