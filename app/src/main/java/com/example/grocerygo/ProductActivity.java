package com.example.grocerygo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.grocerygo.database.AppDatabase;
import com.example.grocerygo.database.Product;
import com.example.grocerygo.database.ProductDoa;

public class ProductActivity extends AppCompatActivity {

    private int value;
    private Toolbar toolbar;

    Button deleteProductButton;

    private TextView title;
    private TextView description;
    private TextView price;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.product_activity);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Product");


        Bundle b = getIntent().getExtras();
        value = -1; // or other values
        if(b != null)
            value = b.getInt("product");

        AppDatabase database = AppDatabase.getInstance(ProductActivity.this);

        final ProductDoa productDoa = database.productDoa();
        final Product product = productDoa.getProductById(value);

        title = (TextView)findViewById(R.id.ProductCardName);
        title.setText(product.getTitle());
        description = (TextView)findViewById(R.id.ProductCardDescription);
        description.setText(product.getDescription());
        price = (TextView)findViewById(R.id.ProductCardPrice);
        price.setText(Double.toString(product.getPrice()));
        deleteProductButton = (Button) findViewById(R.id.deleteProductButton);


        deleteProductButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                productDoa.delete(product);

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });



    }
}