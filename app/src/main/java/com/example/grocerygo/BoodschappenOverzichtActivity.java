package com.example.grocerygo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.grocerygo.database.AppDatabase;
import com.example.grocerygo.database.BoodschappenDoa;
import com.example.grocerygo.database.Boodschappenlijst;

import java.util.ArrayList;
import java.util.List;

public class BoodschappenOverzichtActivity<image> extends AppCompatActivity {
    private Toolbar mTopToolbar;

    RecyclerView recyclerView;

    private CardView cardview;
    private TextView title;
    private TextView description;
    private ImageView buttonTo;
    private ImageButton addBoodschappenlijst;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boodschappenoverzicht_activity);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Grocery Go");

        setSupportActionBar(mTopToolbar);

        cardview = findViewById(R.id.card_view);
        title = findViewById(R.id.Title);
        description = findViewById(R.id.Description);
        addBoodschappenlijst = findViewById(R.id.AddGroceryList);

        addBoodschappenlijst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(BoodschappenOverzichtActivity.this, BoodschappenlijstAanmakenActivity.class), 100);
            }
        });

        getBoodschappenlijst();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 100) {
            Log.d("BACK", "BACK");
            getBoodschappenlijst();
        }
    }

    public void getBoodschappenlijst() {
        AppDatabase database = AppDatabase.getInstance(BoodschappenOverzichtActivity.this);

        BoodschappenDoa boodschappenDoa = database.boodschappenDoa();
        List<Boodschappenlijst> boodschappenlijsts = boodschappenDoa.getBoodschappenlijst();

        List<BoodschappenlijstCard> boodschappenlijstCards = new ArrayList<>();

        for(int i = 0; i < boodschappenlijsts.size(); i++) {
            boodschappenlijstCards.add(
                    new BoodschappenlijstCard(
                            boodschappenlijsts.get(i).getId(),
                            boodschappenlijsts.get(i).getTitle(),
                            boodschappenlijsts.get(i).getBeschrijving()
                    ));
        }

        BoodschappenlijstAdapter adapter = new BoodschappenlijstAdapter(this, boodschappenlijstCards);

        recyclerView.setAdapter(adapter);
    }
}