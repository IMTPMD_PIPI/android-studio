package com.example.grocerygo;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.grocerygo.database.AppDatabase;
import com.example.grocerygo.database.BoodschappenDoa;
import com.example.grocerygo.database.Boodschappenlijst;

public class BoodschappenlijstAanmakenActivity extends AppCompatActivity {

    private EditText BoodschappenlijstNaam;
    private EditText BoodschappenlijstBeschrijving;
    private Button CreeërBoodschappenlijst;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.new_boodschappenlijst_activity);

        BoodschappenlijstNaam = (EditText)findViewById(R.id.boodschappenlijstNaam);
        BoodschappenlijstBeschrijving = (EditText)findViewById(R.id.boodschappenlijstBeschrijving);
        CreeërBoodschappenlijst = (Button) findViewById(R.id.btnCreateBoodschappenlijst);

        CreeërBoodschappenlijst.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String title = BoodschappenlijstNaam.getText().toString();
                String desc = BoodschappenlijstBeschrijving.getText().toString();

                CreeërBoodschappenlijst(title, desc);
            }
        });

    }

    private void CreeërBoodschappenlijst(String title, String desc){

        AppDatabase database = AppDatabase.getInstance(BoodschappenlijstAanmakenActivity.this);

        BoodschappenDoa boodschappenDoa = database.boodschappenDoa();

        Boodschappenlijst boodschappenlijst = new Boodschappenlijst();
        boodschappenlijst.setUserId(1);
        boodschappenlijst.setTitle(title);
        boodschappenlijst.setBeschrijving(desc);

        boodschappenDoa.insert(boodschappenlijst);


        finish();
    }
}
