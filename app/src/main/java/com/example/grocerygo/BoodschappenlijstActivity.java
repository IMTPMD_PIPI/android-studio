package com.example.grocerygo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.example.grocerygo.database.AppDatabase;
import com.example.grocerygo.database.BoodschappenDoa;
import com.example.grocerygo.database.Boodschappenlijst;
import com.example.grocerygo.database.Product;
import com.example.grocerygo.database.ProductDoa;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;

import java.util.ArrayList;
import java.util.List;

public class BoodschappenlijstActivity extends AppCompatActivity {

    RecyclerView recyclerView;

    private int value;

    FloatingActionMenu materialDesignFAM;
    FloatingActionButton floatingActionButton1, floatingActionButton2;

    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.boodschappenlijst_activity);

        toolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Grocery Go");
        Bundle b = getIntent().getExtras();
        value = -1; // or other values
        if(b != null)
            value = b.getInt("id");

        AppDatabase database = AppDatabase.getInstance(BoodschappenlijstActivity.this);

        final BoodschappenDoa boodschappenDoa = database.boodschappenDoa();
        final Boodschappenlijst boodschappenlijst = boodschappenDoa.getBoodschappenlijstById(value);

        recyclerView = (RecyclerView) findViewById(R.id.productView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));


        materialDesignFAM = (FloatingActionMenu) findViewById(R.id.material_design_android_floating_action_menu);
        floatingActionButton1 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item1);
        floatingActionButton2 = (FloatingActionButton) findViewById(R.id.material_design_floating_action_menu_item2);

        floatingActionButton1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                Intent intent = new Intent(BoodschappenlijstActivity.this,AddProductActivity.class);
                Bundle b = new Bundle();
                b.putInt("id", value); //Your id
                intent.putExtras(b); //Put your id to your next Intent
                startActivityForResult(intent, 200);
            }
        });
        floatingActionButton2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                boodschappenDoa.delete(boodschappenlijst);

                Intent intent = new Intent();
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        getProducts();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        if (requestCode == 200) {
            Log.d("BACK", "BACK");
            getProducts();
        }
    }
    public void getProducts(){
        AppDatabase database = AppDatabase.getInstance(BoodschappenlijstActivity.this);

        ProductDoa productDoa = database.productDoa();
        List<Product> products = productDoa.getProductsById(value);

        List<ProductCard> productCards = new ArrayList<>();

        for(int i = 0; i < products.size(); i++) {
            productCards.add(
                    new ProductCard(
                            products.get(i).getId(),
                            products.get(i).getTitle(),
                            products.get(i).getDescription(),
                            products.get(i).getPrice()
                    ));
        }

        ProductAdapter adapter = new ProductAdapter(this, productCards);

        recyclerView.setAdapter(adapter);
    }
}